﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

namespace MTServices
{
	public class FacebookManager : MonoBehaviour {
		public static FacebookManager fbManager = null;

		void Singleton()
		{
			if(fbManager == null)
			{
				fbManager = this;
//				DontDestroyOnLoad (this.gameObject);
			}
			else
			{
				Destroy (this.gameObject);
			}
		}

		void Awake()
		{
			Singleton ();
		}

		// Use this for initialization
		void Start ()
		{		
			//Auto login fb if allowed.	
			if (FBUserDatas.fbData.allowed)
				LoginToGetAvatar ();
		}

		public void OnFacebook()
		{
			if (!FBUserDatas.fbData.allowed)
			{
				LoginToGetAvatar ();
			}
			else
			{
				GotoFacebookPage ();
			}
		}

		public void LoginToGetAvatar()
		{			
			StartCoroutine (Login ());
		}

		public void GotoFacebookPage()
		{
			Application.OpenURL ("https://fb.com/GcenterGames");
		}

		IEnumerator Login()
		{
			yield return new WaitWhile (() => Application.internetReachability == NetworkReachability.NotReachable);
			FBInitialize ();
			yield return new WaitUntil (() => FB.IsInitialized);
			FBLogin ();
			yield break;
		}

		private void FBInitialize()
		{
			FB.Init(this.OnInitComplete, this.OnHideUnity);
		}

		private void FBLogin()
		{
			FB.LogInWithReadPermissions(new List<string>() {"public_profile"}, AuLoginCallback);
		}

		private void CallFBLogout()
		{
			FB.LogOut();
		}

		private static void AuLoginCallback(ILoginResult result)
		{
			if (result.Error != null) {
				print (result.Error); 
				return;
			}
			if (!FBUserDatas.fbData.allowed)
				FBUserDatas.fbData.allowed = true;
			//Dang nhap thanh cong
			//lay thong tin ve ten
			FB.API("/me",HttpMethod.GET,GetUserInfoCallback);
		}

		private  static void GetUserInfoCallback(IGraphResult result){
			if (result.Error != null) {
				print (result.Error);
				return;
			}
			//Lay thong tin nguoi dung thanh cong
			Debug.Log("GetUserName Successfuly!\n" + result.ResultDictionary ["name"]);
			FBUserDatas.fbData.usrInformation.usrName = result.ResultDictionary ["name"].ToString();
			//lay thong tin avatar
			FB.API("/me/picture?type=square&height=128&&width=128",HttpMethod.GET,GetAvatarCallBack);
		}

		private  static void GetAvatarCallBack(IGraphResult result){
			if (result.Error != null) {
				print (result.Error);
				return;
			}

			//Lay avatar thanh cong
			Debug.Log("GetUserSprite Successfuly!");
			FBUserDatas.fbData.usrInformation.usrAvatar = result.Texture;
		}

		private void OnInitComplete()
		{			
			if(FB.IsLoggedIn)
				Debug.Log ("Login successful!");
			else
				Debug.LogWarning ("Failed to Login with Facebook.");				
		}

		private void OnHideUnity(bool isGameShown)
		{			
			Debug.Log ("Is game shown: " + isGameShown);
		}
	}
}
