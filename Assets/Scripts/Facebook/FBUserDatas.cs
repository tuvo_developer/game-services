﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace MTServices
{
	public class FBUserDatas : MonoBehaviour 
	{
		public static FBUserDatas fbData = null;

		void Awake()
		{
			fbData = this;
		}

		public struct Info
		{
			public string usrName;
			public Texture2D usrAvatar;
		}

		public Info usrInformation = new Info();

		public bool allowed
		{
			get
			{
				if (PlayerPrefs.GetInt ("FBLoggedIn") == 1)
					return true;
				else
					return false;
			}

			set
			{
				if(value)
					PlayerPrefs.SetInt ("FBLoggedIn", 1);
				else
					PlayerPrefs.SetInt ("FBLoggedIn", -1);
			}
		}
	}
}