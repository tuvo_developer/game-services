﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

namespace MTServices
{
	/*************************************************************/
	/// <summary>
	/// Google services is class for leaderboard, archiverment.
	/// </summary>
	public class GoogleServices : MonoBehaviour 
	{
		const string HIGHSCORES_LEADERBOARD_ID= "CgkIu8njw50KEAIQAA";
		public static GoogleServices googleServiver = null;

		void Awake ()
		{
			if (googleServiver == null) 
			{
				googleServiver = this;
//				DontDestroyOnLoad (this.gameObject);
			}
			else
			{
				Destroy (this.gameObject);
			}

			StartCoroutine (Init ());
		}

		IEnumerator Init()
		{
			yield return new WaitWhile (() => Application.internetReachability == NetworkReachability.NotReachable);
			Initialization ();
			yield break;
		}

		/// <summary>
		/// Configuration & Initialization Play Game Services
		/// </summary>
		void Initialization()
		{
			if (authenticated)
				return;
			// recommended for debugging:
			PlayGamesPlatform.DebugLogEnabled = true;
			
			PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
				// enables saving game progress.
//				.EnableSavedGames()
				// require access to a player's Google+ social graph (usually not needed)
//				.RequireGooglePlus()
				.Build();

			PlayGamesPlatform.InitializeInstance(config);
			// Activate the Google Play Games platform
			PlayGamesPlatform.Activate();
			SignIn ();
		}

		/// <summary>
		/// Sign in to Google Play Games.
		/// </summary>
		void SignIn() {
			// authenticate user:
			Social.localUser.Authenticate((bool success) => {
				// handle success or failure
				if(success)
				{
					Debug.Log ("Login successful!");
				}				
				else
					Debug.LogWarning ("Failed to sign in with Google Play Games.");
			});
		}

		bool authenticated
		{
			get
			{
				return Social.Active.localUser.authenticated;
			}
		}

		void ReportScore(int score, string leaderboardKey)
		{
			Social.ReportScore(score, leaderboardKey, (bool success) => {
				if(success)
					Debug.Log("Report successful!");
				else
					Debug.LogWarning ("Failed to Report!");
			});
		}

		public void ShowLeaderboard()
		{
			if(authenticated)
				Social.ShowLeaderboardUI ();
		}

		public void ReportHighScores(int highScore)
		{
			if(authenticated)
				Social.ReportScore(highScore, HIGHSCORES_LEADERBOARD_ID, (bool success) => {
					if(success)
						Debug.Log("Report successful!");
					else
						Debug.LogWarning ("Failed to Report!");
				});
		}
	}
}
