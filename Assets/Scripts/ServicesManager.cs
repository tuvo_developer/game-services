﻿using UnityEngine;
using System.Collections;
using MTServices;

public class ServicesManager : MonoBehaviour {
	public static ServicesManager services = null;

	// Use this for initialization
	void Start () 
	{
		if(services == null)
		{
			services = this;
			DontDestroyOnLoad (this.gameObject);
		}
		else
		{
			Destroy (this.gameObject);
		}

		UnityIAP.purchaser.BuySuccess = PurchasedCallBack;
		Invoke ("PlayGame",1.0f);
	}

	public void ByNoAds()
	{
		UnityIAP.purchaser.BuyNonConsumable ();//By Remove Ads item
	}

	public void PurchasedCallBack()
	{
		Debug.Log ("Buy Successfuly");
		GoogleMobileAdsScript.googleMobileAds.RemoveAds ();
	}
}
